#!/usr/bin/env python
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

__license__   = 'GPL v3'
__copyright__ = '2014, Pavol Zibrita <pavol.zibrita@gmail.com>'
__docformat__ = 'restructuredtext sk'

import re
from threading import Thread
import datetime

from calibre.ebooks.metadata.book.base import Metadata
from calibre.ebooks.metadata import check_isbn, normalize_isbn
import calibre_plugins.martinussk.config as cfg

class WorkerMartinus(Thread): # Get details
    isbn = None
    '''
    Get book details from martinus.sk book page in a separate thread
    '''

    def __init__(self, url, result_queue, browser, log, relevance, plugin, lang_in_title, timeout=20):
        Thread.__init__(self)
        self.daemon = True
        self.url, self.result_queue = url, result_queue
        self.log, self.timeout = log, timeout
        self.relevance, self.plugin = relevance, plugin
        self.browser = browser.clone_browser()
        self.cover_url = self.martinussk_id = None
        self.lang_in_title = lang_in_title

    def run(self):
        try:
            self.get_details()
        except:
            self.log.exception('get_details failed for url: %r'%self.url)

    def get_details(self):
        from calibre_plugins.martinussk import load_url
        try:
            self.log.debug('Running load_url for: %s ' % self.url )
            root = load_url(self.log, self.url, self.browser)
            self.parse_details(root)
        except Exception as e:
            self.log.error('Load url problem: %r - %s' % (self.url, e))
        return 

    def parse_details(self, root):
        append_original_title = cfg.plugin_prefs[cfg.STORE_NAME].get(cfg.APPEND_ORIGINAL_TITLE, cfg.DEFAULT_STORE_VALUES[cfg.APPEND_ORIGINAL_TITLE])
        append_translators = cfg.plugin_prefs[cfg.STORE_NAME].get(cfg.APPEND_TRANSLATION, cfg.DEFAULT_STORE_VALUES[cfg.APPEND_TRANSLATION])

        try:
            self.log.info('Getting identifier from: %s' % self.url)
            martinussk_id = self.parse_martinussk_id(self.url)
            self.log.info('Parsed martinus identifier:%s' % martinussk_id)
            title = self.parse_title(root)
            self.log.info('Parsed title:%s'%title)
            authors = self.parse_authors(root)
            self.log.info('Parsed authors:%s'%authors)
        except Exception as e:
            self.log.exception('Error parsing martinussk id, authors or title for url: %r with excception:' % (self.url,e))
            return

        language = self.parse_language(root)
        showing_title = title if not self.lang_in_title else title + ' (' + language + ')'
        mi = Metadata(showing_title, authors)
        self.log.info('dbki:%s'%martinussk_id)
        mi.get_identifiers()['martinussk'] = martinussk_id
        self.martinussk_id = martinussk_id

        mi.series = self.parse_series(root)
        mi.comments = self.parse_comments(root)
        mi.tags = self.parse_tags(root)
        mi.rating = self.parse_rating(root)
        mi.pubdate = self.parse_date(root)
        mi.languages = [self.parse_language(root)]
        mi.publisher = self.parse_publisher(root)
        parse_isbn = self.parse_isbn(root)
        if len(parse_isbn) == 10:
            parse_isbn = '978' + parse_isbn
        normalized_isbn = normalize_isbn(parse_isbn)
        mi.isbn = self.isbn = check_isbn(normalized_isbn)
        self.log.info("ISBN parsed:%s normalized:%s checked:%s" % (parse_isbn, normalized_isbn, self.isbn))
        # mi.isbn = self.isbn = self.parse_isbn(root)
        mi.source_relevance = self.relevance
        mi.has_cover = self.cache_cover(root, self.martinussk_id)
        mi.original_title = "Hello"
        if append_original_title:
            parsed_original_title = self.parse_original_title(root)
            if parsed_original_title:
                if not mi.comments:
                    mi.comments = ""
                mi.comments += '<p id="original_title">Original title: %s </p>' % parsed_original_title
                mi.set_user_metadata('#original_title', {'datatype': 'text',
                                                         'name': 'original_title',
                                                         'is_multiple': False,
                                                         '#value#': None})
                mi.set('#original_title', parsed_original_title)
        if append_translators:
            parsed_translation = self.parse_translation(root)
            if parsed_translation:
                if not mi.comments:
                    mi.comments = ""
                mi.comments += '<p id="translation">Translation: %s </p>' % parsed_translation
                mi.set_user_metadata('#translation', {'datatype': 'text',
                                                      'name': 'translation',
                                                      'is_multiple': False,
                                                      '#value#': None})
                mi.set('#translation', parsed_translation)

        if self.isbn:
            self.plugin.cache_isbn_to_identifier(self.isbn, self.martinussk_id)

        self.log.info(mi)
        self.result_queue.put(mi)

    def parse_martinussk_id(self, url):
        return re.search('sk\/(.*)\/kniha', url).groups(0)[0]
        
    def parse_first(self, root, xpath, loginfo, convert=lambda x: x[0].strip()):
        try:
            nodes = root.xpath(xpath)
            self.log.info('Found %s: %s' % (loginfo,nodes))
            return convert(nodes) if nodes else None
        except Exception:
            self.log.exception('Error parsing for %s with xpath: %s' % (loginfo, xpath))

    def parse_all(self, root, xpath, loginfo, convert=lambda x: [node.strip() for node in x]):
        try:
            nodes = root.xpath(xpath)
            self.log.info('Found %s: %s' % (loginfo,','.join(nodes)))
            return convert(nodes) if nodes else None
        except Exception:
            self.log.exception('Error parsing for %s with xpath: %s' % (loginfo, xpath))

    def parse_title(self, root):
        return self.parse_first(root,'//h1[contains(@class,"product-detail__title")]//text()','title', 
            lambda x: x[0].replace('&nbsp;','').replace('\n','').strip())
            
    def parse_series(self, root):
        return self.parse_first(root, '//a[contains(@href,"edicia")]//text()', 'series')
        
    def parse_authors(self, root):
        return self.parse_all(root, '//ul[contains(@class,"product-detail__author")]//a[contains(@href,"author")]//text()', 'authors')

    def parse_tags(self, root):
        return self.parse_all(root, '//dl[child::dt[contains(text(),"Téma")]]//dd//a//text()', 'tags')
            
    def parse_comments(self, root):
        return self.parse_first(root,'string(//section[@id="description"]//div[@class="cms-article"])', 'comments',
            lambda x: x.strip())
    
    def parse_isbn(self, root):
        return self.parse_first(root,'//dl[child::dt[contains(text(),"ISBN")]]//dd//text()', 'ISBN')
            
    def parse_date(self,root):
        try:
            date = root.xpath('//*[@id="itemFullSpecification"]//li[child::strong[contains(text(),"Dátum vydania")]]/text()')
            self.log.info('Parsing published: %s' % date)
            if not date:
                date = root.xpath('//dl[child::dt[contains(text(),"Rok vydania")]]//dd//text()')
                self.log.info('Parsing published year: %s' % date)
                return datetime.datetime.strptime(date[0].strip(),'%Y') if date else None
            return datetime.datetime.strptime(date[0].strip(),'%d.%m.%Y') if date else None
        except Exception as e:
            self.log.exception('Error parsing for date with exception: %s' % e)    

    def parse_language(self, root):
        return self.parse_first(root,
            '//dl[child::dt[contains(text(),"Jazyk")]]//dd//text()',
            'language', 
            lambda x: x[0].strip())

    def parse_original_title(self, root):
        return self.parse_first(root,
                                '//dl[child::dt[contains(text(),"Originálny názov")]]//dd//text()',
                                'original title',
                                lambda x: x[0].strip())

    def parse_translation(self, root):
        return self.parse_first(root,
                                '//dl[child::dt[contains(text(),"Preklad")]]//dd//text()',
                                'translation',
                                lambda x: x[0].strip())

    def parse_publisher(self, root):
        return self.parse_first(root,'//a[contains(@href,"vydavatelstvo")]//text()','publisher')
        
    def parse_rating(self, root):
        rating = self.parse_first(root,
                                '//a[contains(@href,"#reviews")]//span//text()','rating',
                                lambda x: x[0].replace(',','.').strip())
        if not rating:
            rating = self.parse_first(root,
                                      '//div[contains(@class,"rating-text__value")]//span/text()','rating',
                                      lambda x: x[0].replace(',','.').strip())
        if rating:
            try:
                rating = float(rating)
                self.log.info('Parsed rating: %f' % rating)
            except ValueError as e:
                rating = None
                self.log.info('Parsed rating failed %s' % e)
        return rating
    
    def parse_cover(self, root):
        book_cover = root.xpath('//div[contains(@class,"product-detail__image")]//img/@src')
        self.log.info('Cover: %s' % book_cover)
        if book_cover:
            return 'https:' + book_cover[0]
        else: 
            return None

    def cache_cover(self, root, martinus_id): 
        try:
            self.cover_url = self.parse_cover(root)
            self.log.info('Parsed URL for cover:%r'%self.cover_url)
            self.plugin.cache_identifier_to_cover_url(martinus_id, self.cover_url)
        except Exception as e:
            self.log.exception('Error parsing cover for url: %r with exception: %s' % (self.url, e))
        return bool(self.cover_url)
