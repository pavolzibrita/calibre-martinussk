#!/usr/bin/env python
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

__license__   = 'GPL v3'
__copyright__ = '2021, Pavol Zibrita <pavol.zibrita+calibre@gmail.com>'
__docformat__ = 'restructuredtext sk'

import time

try:
    from queue import Empty, Queue
except ImportError:
    from Queue import Empty, Queue
from six import text_type as unicode

from lxml.html import fromstring
from calibre import as_unicode
from calibre.ebooks.metadata import check_isbn
from calibre.ebooks.metadata.sources.base import Source
from calibre.utils.cleantext import clean_ascii_chars
import lxml, sys, traceback
from calibre import browser
import urllib

def load_url(log, query, br):
    try:
        log.info('Querying: %s' % query)
        response = br.open(str(query))
    except Exception as e:
        log.exception(e)
        raise Exception('Failed to make identify query: %r - %s ' % (query,e))

    try:
        raw = response.read().strip()
        if not raw:
            log.error('Failed to get raw result for query: %r'%query)
            raise Exception('Failed to get raw result for query: %r'%query)
        root = fromstring(clean_ascii_chars(raw))
    except:
        msg = 'Failed to parse martinussk page for query: %r'%query
        log.exception(msg)
        raise Exception(msg)
    return root

class martinussk(Source):
    name                    = 'Martinus.sk'
    description             = _('Downloads metadata and covers from martinus.sk')
    author                  = 'Pavol Zibrita'
    version                 = (2024, 1, 21)
    minimum_calibre_version = (7, 4, 0)

    capabilities = frozenset(['identify', 'cover'])
    touched_fields = frozenset(['title', 'authors', 'identifier:martinussk',
                                'identifier:isbn', 'rating', 'comments', 'publisher', 'pubdate',
                                'series', 'tags', 'languages'])
    has_html_comments = False
    supports_gzip_transfer_encoding = False
    can_get_multiple_covers = False
    ignore_ssl_errors = True

    BASE_URL = "https://www.martinus.sk/"

    def config_widget(self):
        '''
        Overriding the default configuration screen for our own custom configuration
        '''
        from calibre_plugins.martinussk.config import ConfigWidget
        return ConfigWidget(self)

    def get_book_url(self, identifiers):
        martinussk_id = identifiers.get('martinussk', None)
        if martinussk_id:
            return ('martinussk', martinussk_id,
                    '%s?uItem=%s'%(martinussk.BASE_URL, martinussk_id))

    def create_query(self, log, title=None, authors=None):
        if title is not None:
            search_title = title.replace(' ', '+')
        else:
            search_title = ''

        if authors is not None:
            search_author = authors[0].replace(' ', '+')
        else:
            search_author = ''

        search_page = 'https://www.martinus.sk/?uMod=list&uTyp=search&c=čšťď&uQ=%s+%s' % (search_title,search_author)
        return search_page

    def get_cached_cover_url(self, identifiers):
        url = None
        martinussk_id = identifiers.get('martinussk', None)
        if martinussk_id is None:
            isbn = check_isbn(identifiers.get('isbn', None))
            if isbn is not None:
                martinussk_id = self.cached_isbn_to_identifier(isbn)
        if martinussk_id is not None:
            url = self.cached_identifier_to_cover_url(martinussk_id)
            return url

    def identify(self, log, result_queue, abort, title, authors, identifiers={}, timeout=30):
        '''
        Note this method will retry without identifiers automatically if no
        match is found with identifiers.
        '''
        import calibre_plugins.martinussk.config as cfg
        lang_in_title = cfg.plugin_prefs[cfg.STORE_NAME].get(cfg.KEY_INCLUDE_LANGUAGE_IN_TITLES, True)

        matches = []
        martinussk_id = identifiers.get('martinussk', None)
        log.info(u'\nTitle:%s\nAuthors:%s\n'%(title, authors))
        br = browser()
        if martinussk_id:
            matches.append(martinussk.BASE_URL + '?uItem=' + martinussk_id)
        else:
            query = self.create_query(log, title=title, authors=authors)
            if query is None:
                log.error('Insufficient metadata to construct query')
                return
            log.info('Running for query: %s' % query)
            try:
                root = load_url(log, query, br)
            except Exception as e:
                return as_unicode(e)
            self._parse_search_results(log, title, authors, root, matches, timeout)

        if abort.is_set():
            log.info("Abort is set to true, aborting")
            return

        if not matches:
            if identifiers and (title or authors):
                log.info('No matches found with identifiers, retrying using only'
                        ' title and authors')
                return self.identify(log, result_queue, abort, title=title,
                        authors=authors, timeout=timeout)
            log.error('No matches found with query: %r'%query)
            return

        log.debug('Starting workers for: %s' % (matches,))
        from calibre_plugins.martinussk.worker import WorkerMartinus
        workers = [WorkerMartinus(url, result_queue, br, log, i, self, lang_in_title) for i, url in
                enumerate(matches) if url]

        for w in workers:
            w.start()
            time.sleep(0.1)

        a_worker_is_alive = True
        while not abort.is_set() and a_worker_is_alive:
            log.debug('Waiting for workers')
            a_worker_is_alive = False
            for w in workers:
                w.join(0.2)
                if abort.is_set():
                    break
                a_worker_is_alive |= w.is_alive()

        return None

    def _parse_search_results(self, log, orig_title, orig_authors, root, matches, timeout):
        if orig_authors is None:
            orig_authors = []
        results = root.xpath('//div[contains(@class,"listing__item__details")]')
        import calibre_plugins.martinussk.config as cfg
        max_results = cfg.plugin_prefs[cfg.STORE_NAME][cfg.KEY_MAX_DOWNLOADS]
        no_matches = []
        for result in results:
            log.info('Parsing result: %s ' % result )
            title = result.xpath('*//span[contains(@class,"link link--product")]//text()')
            book_url = result.xpath('*//a[contains(@href,"kniha")]/@href')
            author = result.xpath('*//a[contains(@href,"authors")]//text()')
            if not title or not book_url or not author:
                log.info("Missing at least on of: title, book url or author, skipping. Title: %s, Url: %s, Author: %s" % (str(title),str(book_url),str(author)))
                continue
            title = title[0]
            book_url = book_url[0]
            if book_url.startswith('//'):
                book_url = "https:" + book_url
            if book_url.startswith('/?'):
                book_url = self.BASE_URL + book_url
            if book_url.startswith('/'):
                book_url = self.BASE_URL + book_url[1:]
            author = author[0]
            log.info('Book: %s\nOriginal author: %s\nAuthor:%s\nBook url: %s' % (title, orig_authors, author, book_url))
            if self.match(title,orig_title) or (author in orig_authors) or (self.match(author,oauthor) for oauthor in orig_authors):
                matches.append(book_url)
            else:
                no_matches.append(book_url)
            if len(matches) >= max_results:
                break
        if no_matches and not matches:
            matches.extend(no_matches)

    def match(self, item1, item2):
        return item1 == item2 or item1 in item2 or item2 in item1

    def download_cover(self, log, result_queue, abort, title=None, authors=None, identifiers={}, timeout=30):
        cached_url = self.get_cached_cover_url(identifiers)
        if cached_url is None:
            log.info('No cached cover found, running identify')
            rq = Queue()
            self.identify(log, rq, abort, title=title, authors=authors, identifiers=identifiers)
            if abort.is_set():
                return
            results = []
            while True:
                try:
                    results.append(rq.get_nowait())
                except Empty:
                    break
            results.sort(key=self.identify_results_keygen(
                title=title, authors=authors, identifiers=identifiers))
            for mi in results:
                cached_url = self.get_cached_cover_url(mi.identifiers)
                if cached_url is not None:
                    break
        if cached_url is None:
            log.info('No cover found')
            return

        if abort.is_set():
            return
        br = self.browser
        log.info('Downloading cover from:', cached_url)
        try:
            cdata = br.open_novisit(cached_url, timeout=timeout).read()
            result_queue.put((self, cdata))
        except:
            log.exception('Failed to download cover from:', cached_url)
