import logging as log
from calibre.utils.cleantext import clean_ascii_chars
from calibre import browser
from calibre_plugins.martinussk import load_url

if __name__ == '__main__':
    br = browser()
    from calibre import ipython
    ipython(locals())
