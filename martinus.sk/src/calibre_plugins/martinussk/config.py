#!/usr/bin/env python
# vim:fileencoding=UTF-8:ts=4:sw=4:sta:et:sts=4:ai
from __future__ import (unicode_literals, division, absolute_import,
                        print_function)

__license__   = 'GPL v3'
__copyright__ = '2021, Pavol Zibrita <pavol.zibrita@gmail.com>'
__docformat__ = 'restructuredtext sk'

from six import text_type as unicode
try:
    from PyQt5.Qt import QLabel, QVBoxLayout, QHBoxLayout, Qt, QGroupBox, QSpinBox, QCheckBox, QWidget
except:
    from PyQt4.Qt import QLabel, QVBoxLayout, QHBoxLayout, Qt, QGroupBox, QSpinBox, QCheckBox, QWidget

from calibre.gui2.metadata.config import ConfigWidget as DefaultConfigWidget
from calibre.utils.config import JSONConfig

STORE_NAME = 'Options'
KEY_MAX_DOWNLOADS = 'maxDownloads'
KEY_INCLUDE_LANGUAGE_IN_TITLES = 'includeLanguageInTitle'
APPEND_ORIGINAL_TITLE = 'appendOriginalTitle'
APPEND_TRANSLATION = 'appendTranslation'

DEFAULT_STORE_VALUES = {
    KEY_MAX_DOWNLOADS: 10,
    KEY_INCLUDE_LANGUAGE_IN_TITLES: True,
    APPEND_ORIGINAL_TITLE: False,
    APPEND_TRANSLATION: False
}

# This is where all preferences for this plugin will be stored
plugin_prefs = JSONConfig('plugins/martinussk')

# Set defaults
plugin_prefs.defaults[STORE_NAME] = DEFAULT_STORE_VALUES

class ConfigWidget(DefaultConfigWidget):

    def __init__(self, plugin):
        DefaultConfigWidget.__init__(self, plugin)
        c = plugin_prefs[STORE_NAME]

        other_group_box = QGroupBox('Other options', self)
        self.l.addWidget(other_group_box, self.l.rowCount(), 0, 1, 2)
        other_group_box_layout = QVBoxLayout()
        other_group_box_layout.setAlignment(Qt.AlignTop)
        other_group_box.setLayout(other_group_box_layout)

        max_spins_group = QWidget(other_group_box)
        horizonal_layout = QHBoxLayout()
        max_spins_group.setLayout(horizonal_layout)
        max_label = QLabel('Maximum title/author search matches to evaluate (1 = fastest):', self)
        max_label.setToolTip('Number of searches to consider')
        horizonal_layout.addWidget(max_label)
        self.max_downloads_spin = QSpinBox(self)
        self.max_downloads_spin.setMinimum(5)
        self.max_downloads_spin.setMaximum(50)
        self.max_downloads_spin.setProperty('value', c.get(KEY_MAX_DOWNLOADS, DEFAULT_STORE_VALUES[KEY_MAX_DOWNLOADS]))
        horizonal_layout.addWidget(self.max_downloads_spin)
        horizonal_layout.insertStretch(-1)

        other_group_box_layout.addWidget(max_spins_group)
        self.include_lang = QCheckBox("Include language in book title", self)
        self.include_lang.setToolTip("If checked, search results will be not merged, for the same book and different language")
        self.include_lang.setChecked(c.get(KEY_INCLUDE_LANGUAGE_IN_TITLES, DEFAULT_STORE_VALUES[KEY_INCLUDE_LANGUAGE_IN_TITLES]))
        other_group_box_layout.addWidget(self.include_lang)

        self.aoriginaltitle_check = QCheckBox(_('Original title'), self)
        self.aoriginaltitle_check.setToolTip(_('If the information about the book will contain the original title\n'
                                               '- it will be appended at the end of the comment.'))
        self.aoriginaltitle_check.setChecked(c.get(APPEND_ORIGINAL_TITLE, DEFAULT_STORE_VALUES[APPEND_ORIGINAL_TITLE]))
        other_group_box_layout.addWidget(self.aoriginaltitle_check)

        self.atranslation_check = QCheckBox(_('Translator(s)'), self)
        self.atranslation_check.setToolTip(_('If the information about the book will contain info about translators\n'
                                             '- it will be appended at the end of the comment.'))
        self.atranslation_check.setChecked(c.get(APPEND_TRANSLATION, DEFAULT_STORE_VALUES[APPEND_TRANSLATION]))
        other_group_box_layout.addWidget(self.atranslation_check)

    def commit(self):
        DefaultConfigWidget.commit(self)
        new_prefs = {}
        new_prefs[KEY_MAX_DOWNLOADS] = int(unicode(self.max_downloads_spin.value()))
        new_prefs[KEY_INCLUDE_LANGUAGE_IN_TITLES] = self.include_lang.isChecked()
        new_prefs[APPEND_ORIGINAL_TITLE] = self.aoriginaltitle_check.isChecked()
        new_prefs[APPEND_TRANSLATION] = self.atranslation_check.isChecked()
        plugin_prefs[STORE_NAME] = new_prefs
