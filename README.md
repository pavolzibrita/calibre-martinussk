# README #

### What is this repository for? ###

* Calibre plugin for metadata download from martinus.sk
* Latest version: 2024.1.21
* [Plugin forum page](https://www.mobileread.com/forums/showthread.php?p=2767819)
* List of calibre plugins: https://plugins.calibre-ebook.com/

### How do I get set up? ###

* See https://manual.calibre-ebook.com/creating_plugins.html#pluginstutorial

### Contribution guidelines ###

* https://bitbucket.org/pavolzibrita/calibre-martinussk/src/master/
* Create pull request

### Who do I talk to? ###

* Use the plugin's forum page

### Debugging plugin ###

* https://manual.calibre-ebook.com/creating_plugins.html#debugging-plugins
```bash
calibre-debug -s; calibre-customize -b E:\Projects\calibre-martinussk\martinus.sk\src\calibre_plugins\martinussk; calibre
```